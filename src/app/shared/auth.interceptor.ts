import { HttpInterceptor } from '@angular/common/http';
import { AuthService } from '../components/auth/auth.service';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, AuthState } from '../store/app.reducers';
import { switchMap,take } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
    constructor(private store:Store<AppState>){}

    intercept(req: import("@angular/common/http").HttpRequest<any>, next: import("@angular/common/http").HttpHandler): import("rxjs/Observable").Observable<import("@angular/common/http").HttpEvent<any>> {
        console.log('Intercepted',req);

        return this.store.select('auth').pipe(take(1),switchMap((authState:AuthState)=>{
            const copiedReq=req.clone({
                headers:req.headers.append('token','token'),
                params:req.params.set('auth',authState.token)
            });
            return next.handle(copiedReq);
        }))
    }
    // intercept(req: import("@angular/common/http").HttpRequest<any>, next: import("@angular/common/http").HttpHandler): import("rxjs/Observable").Observable<import("@angular/common/http").HttpEvent<any>> {
    //     console.log('Intercepted',req);
    //     const token = this.authService.getToken();

    //     //request are immutable
    //     const copiedReq=req.clone({
    //         headers:req.headers.append('token','token'),
    //         params:req.params.set('auth',token)
    //     });
    //     // req.headers.append('auth',"token")
    //     return next.handle(copiedReq)
    // }

}