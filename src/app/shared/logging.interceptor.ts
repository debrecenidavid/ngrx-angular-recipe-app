import { HttpInterceptor } from '@angular/common/http';
import 'rxjs/add/operator/do'

export class LoggingInterceptor implements HttpInterceptor{
    intercept(req: import("@angular/common/http").HttpRequest<any>, next: import("@angular/common/http").HttpHandler): import("rxjs").Observable<import("@angular/common/http").HttpEvent<any>> {
        return next.handle(req).do(
            event=>{
                console.log('Logging interceptor',event);
            }
        )
    }
    
}