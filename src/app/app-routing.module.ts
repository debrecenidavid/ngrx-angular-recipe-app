import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ShoppingListComponent } from './components/shopping/shopping-list/shopping-list.component';
import { AuthGuard } from './components/auth/auth-guard.service';
import { HomeComponent } from './components/core/home/home.component';

const appRoutes: Routes = [
  // {path:'',redirectTo:'/recipes', pathMatch:'full'},
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'recipes', loadChildren: './components/recipes/recipes.module#RecipesModule', canLoad: [AuthGuard] },//Lazy loading canActivate still works
  { path: 'shopping-list', component: ShoppingListComponent, canActivate: [AuthGuard] },
]
@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule] // if export here just like u import in app.module
})
export class AppRoutingModule {

}