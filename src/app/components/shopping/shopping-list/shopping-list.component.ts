import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as ShoppingActions from '../store/shopping.actions'
import { ShoppingState, AppState } from 'src/app/store/app.reducers';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {

  // ingredients: Ingredient[] change to NGRX version
  shoppingState: Observable<ShoppingState>
  // private subscription: Subscription
  constructor(private store:Store<AppState>
    ) { }

  ngOnInit() {
    // this.ingredients = this.shoppingService.getIngredients(); change to NGRX version
    // this.subscription = this.shoppingService.ingredientsChange.subscribe(  change to NGRX version
    //   (ingredients: Ingredient[]) => { this.ingredients = ingredients }
    // );
    this.shoppingState = this.store.select('shopping'); //NGRX

  }

  onEditItem(index: number) {
    // this.shoppingService.startedEditing.next(index);
    this.store.dispatch(new ShoppingActions.StartEdit({index}))
  }

  // ngOnDestroy(): void {
  //   this.subscription.unsubscribe();
  //   console.log('destroyed');
  // }

}
