import { Ingredient } from 'src/app/shared/ingredient.model';
import * as ShoppingActions from './shopping.actions'
import { ShoppingState } from 'src/app/store/app.reducers';

const initialState: ShoppingState = {
    ingredients: [
        new Ingredient('Apples', 2),
        new Ingredient('Tomatoes', 10),
        new Ingredient('Milk', 1),
    ],
    editedIngredient: null,
    editedIngredientIndex: -1
};
export function shoppingReducer(state = initialState, action: ShoppingActions.ShoppingActionsType) {
    switch (action.type) {
        case ShoppingActions.ADD_INGREDIENT:
            return {
                ...state,  // foreach the old state and return all elemnet
                ingredients: [...state.ingredients, action.payload]// change the ingredients
            }
        case ShoppingActions.ADD_INGREDIENTS:
            return {
                ...state,
                ingredients: [...state.ingredients, ...action.payload]//add all step by step ...(spread) operator
            }
        case ShoppingActions.UPDATE_INGREDIENT:
            const oldIngredient = state.ingredients[state.editedIngredientIndex];
            const updatedIngredients = {
                ...oldIngredient,
                ...action.payload.newIngredient// override with a new
            }
            const ingredients = [...state.ingredients];// gives a copy for us like slice()
            ingredients[state.editedIngredientIndex] = updatedIngredients;
            return {
                ...state,
                ingredients: ingredients,//add all step by step ...(spread) operator
                editedIngredient:null,
                editedIngredientIndex:-1
            }
        case ShoppingActions.DELETE_INGREDIENT:
            const ingredientsArray = [...state.ingredients];
            ingredientsArray.splice(state.editedIngredientIndex, 1);
            return {
                ...state,
                ingredients: ingredientsArray,//add all step by step ...(spread) operator
                editedIngredient:null,
                editedIngredientIndex:-1
            }
        case ShoppingActions.START_EDIT:
            const editedIngredient = { ...state.ingredients[action.payload.index] }
            return {
                ...state,
                editedIngredientIndex: action.payload.index,
                editedIngredient:editedIngredient
            }
        case ShoppingActions.STOP_EDIT:
            return {
                ...state,
                editedIngredient:null,
                editedIngredientIndex:-1
            }
        default:
            return state;
    }
}