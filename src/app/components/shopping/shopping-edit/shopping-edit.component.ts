import { Component, OnInit, ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';

import { Ingredient } from 'src/app/shared/ingredient.model';
import * as ShoppingActions from '../store/shopping.actions'
import { AppState, ShoppingState } from 'src/app/store/app.reducers';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @ViewChild('f')slForm:NgForm;
  subscription:Subscription;
  editMode=false;
  editedItem:Ingredient;

  constructor(private store:Store<AppState>) { }

  ngOnInit() {
    this.subscription = this.store.select('shopping').subscribe(
      (data:ShoppingState) =>{
        if(data.editedIngredientIndex > -1){
          this.editedItem = data.editedIngredient;
          this.editMode = true;
          this.slForm.setValue({
            name:this.editedItem.name,
            amount:this.editedItem.amount
          })
        }else{
          this.editMode = false;
        }
      }
    )
  }

  onSubmit(form:NgForm) {
    const newIngredient=new Ingredient(form.value.name,form.value.amount)
    if(this.editMode){
      this.store.dispatch(new ShoppingActions.UpdateIngredient({
        newIngredient
      }));
    }else{
      this.store.dispatch(new ShoppingActions.AddIngredient(newIngredient));//NGRX
    }
    this.onClear();
  }

  onDelete(){
    this.store.dispatch(new ShoppingActions.DeleteIngredient());
    this.onClear();
  }

  onClear(){
    this.editMode=false;
    this.store.dispatch(new ShoppingActions.StopEdit());
    this.slForm.reset();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.store.dispatch(new ShoppingActions.StopEdit());
  }
}
