import { AuthState } from 'src/app/store/app.reducers';
import * as AuthActions from './auth.actions'
const initialState: AuthState = {
    token: null,
    authenticated: false
}

export function authReducer(state = initialState, action:AuthActions.AuthActionsType) {
    switch (action.type) {
        case AuthActions.SINGUP:
        case AuthActions.SINGIN:
            return {
                ...state,
                authenticated:true
            }
        case AuthActions.LOGOUT:
            return {
                ...state,
                authenticated:false,
                token:null
            }
        case AuthActions.SET_TOKEN:
            return {
                ...state,
                token:action.payload
            }
        default:
            return state;
    }
}