import { Action } from '@ngrx/store'

export const TRY_SIGNUP = 'TRY_SIGNUP';
export const SINGUP = 'SINGUP'
export const SINGIN = 'SINGIN'
export const TRY_SIGNIN = 'TRY_SIGNIN';
export const LOGOUT = 'LOGOUT'
export const SET_TOKEN = 'SET_TOKEN'

export class TrySignUp implements Action {
    readonly type = TRY_SIGNUP;
    constructor(public payload:{username:string,password:string}){}
}
export class SingUp implements Action {
    readonly type = SINGUP;
}
export class SingIn implements Action {
    readonly type = SINGIN;
}
export class TrySignIn implements Action {
    readonly type = TRY_SIGNIN;
    constructor(public payload:{username:string,password:string}){}
}
export class Logout implements Action {
    readonly type = LOGOUT;
}
export class SetToken implements Action {
    readonly type = SET_TOKEN;
    constructor(public payload: string){}
}

export type AuthActionsType =
TrySignUp|
SingUp |
SingIn |
TrySignIn |
Logout |
SetToken;
