import { Effect, Actions, ofType } from '@ngrx/effects'
import { Injectable } from '@angular/core';
import * as AuthActions from './auth.actions'
// import { map, switchMap, mergeMap } from 'rxjs/operators'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/switchMap'
import 'rxjs/add/operator/mergeMap'
import { from } from 'rxjs'
import * as firebase from 'firebase'
import { Router } from '@angular/router';
@Injectable()
export class AuthEffects {
    urlAfterLogin: string = '/'
    @Effect()
    authSignUp = this.actions$.pipe(ofType(AuthActions.TRY_SIGNUP))
        .map((action: AuthActions.TrySignUp) => {
            return action.payload;
        })
        .switchMap((authData: { username: string, password: string }) => {
            return from(firebase.auth().createUserWithEmailAndPassword(authData.username, authData.password))
        })
        .switchMap(() => {
            return from(firebase.auth().currentUser.getIdToken())
        })
        .mergeMap((token: string) => {
            this.router.navigate(['/']);
            return [
                {
                    type: AuthActions.SINGUP
                },
                {
                    type: AuthActions.SET_TOKEN,
                    payload: token
                }
            ]
        })

    @Effect()
    authSignIn = this.actions$.pipe(
        ofType(AuthActions.TRY_SIGNIN))
        .map((action: AuthActions.TrySignIn) => {
            return action.payload;
        })
        .switchMap((authData: { username: string, password: string }) => {
            return from(firebase.auth().signInWithEmailAndPassword(authData.username, authData.password))
        })
        .switchMap(() => {
            return from(firebase.auth().currentUser.getIdToken())
        })
        .mergeMap((token: string) => {
            this.router.navigate([this.urlAfterLogin])
            return [
                {
                    type: AuthActions.SINGIN
                },
                {
                    type: AuthActions.SET_TOKEN,
                    payload: token
                }
            ]
        })

    @Effect({ dispatch: false })
    authLogout = this.actions$.pipe(ofType(AuthActions.LOGOUT))
        .do(() => {
            this.router.navigate(['/']);
        })

    @Effect({ dispatch: false })
    nav = this.actions$.pipe(ofType("@ngrx/router-store/request")).take(1)
        .map((data: any) => {
            let url = data.payload.event.url
            if (url !== '/signin') {
                console.log(url)
                this.urlAfterLogin = url;
            }
        })

    constructor(private actions$: Actions, private router: Router) { }

}