import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanLoad, Route, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Store } from '@ngrx/store';
import { AppState, AuthState } from 'src/app/store/app.reducers';
// import { map, take } from 'rxjs/operators';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {
    constructor(private authService: AuthService, private router: Router, private store: Store<AppState>) { }
    canLoad(route: Route, segments: UrlSegment[]): boolean | Observable<boolean> | Promise<boolean> {
        let path = "";
        segments.map(s => {
            path += `/${s.path}`
        })
        this.authService.urlAfterLogin = path;

        const result: Observable<boolean> = this.store.select('auth').take(1).map(
            (authState: AuthState) => {
                return authState.authenticated
            });
        result.subscribe(data => {
            if (data) {
                return true;
            } else {
                let path = "";
                segments.map(s => {
                    path += `/${s.path}`
                })
                this.authService.urlAfterLogin = path;
                this.router.navigate(['/signin']);
            }
        })
        return result;
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        const result: Observable<boolean> = this.store.select('auth').take(1).map(
            (authState: AuthState) => {
                return authState.authenticated
            });
        result.subscribe(data => {
            if (data) {
                return true;
            } else {
                this.authService.urlAfterLogin = state.url;
                this.router.navigate(['/signin']);
            }
        })
        return result;
    }
    // canLoad(route:Route, segments:UrlSegment[]): boolean | Observable<boolean> | Promise<boolean> {
    //     if(this.authService.isAuthenticated()){
    //         return true;
    //     }else{
    //         let path="";
    //         segments.map(s=>{
    //             path+=`/${s.path}`
    //         })
    //         this.authService.urlAfterLogin=path;
    //         this.router.navigate(['/signin']);
    //     }
    //     return this.authService.isAuthenticated();
    // }

    // canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    //     if(this.authService.isAuthenticated()){
    //         return true;
    //     }else{
    //         this.authService.urlAfterLogin=state.url;
    //         this.router.navigate(['/signin']);
    //     }
    //     return this.authService.isAuthenticated();
    // }


}