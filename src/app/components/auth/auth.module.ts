import { NgModule } from "@angular/core";
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { FormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
// import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        SignupComponent,
        SigninComponent,
    ],
    imports: [
        FormsModule,
        // CommonModule,
        AuthRoutingModule
    ]
})
export class AuthModule { }