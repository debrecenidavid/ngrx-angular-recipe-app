import * as firebase from 'firebase/app';
import 'firebase/auth';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducers';
import * as AuthActions from './store/auth.actions';

@Injectable()
export class AuthService {

    urlAfterLogin = '/';

    constructor(private router: Router, private store: Store<AppState>) { }
    signupUser(email: string, password: string) {
        firebase.auth()
            .createUserWithEmailAndPassword(email, password)
            .then(
                user => {
                    this.store.dispatch(new AuthActions.SingUp())
                    firebase.auth().currentUser.getIdToken()
                        .then(token => this.store.dispatch(new AuthActions.SetToken(token)))
                        .catch(err => console.log(err))
                    this.router.navigate(['/']);
                }
            )
            .catch(err => console.log(err)
            )
    }

    signInUser(email: string, password: string) {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(res => {
                this.store.dispatch(new AuthActions.SingIn())
                // firebase.auth().currentUser.getIdToken().then(token => this.token = token).catch(err => console.log(err));
                firebase.auth().currentUser.getIdToken()
                    .then(token => this.store.dispatch(new AuthActions.SetToken(token)))
                    .catch(err => console.log(err))
                this.router.navigate([this.urlAfterLogin]);
            })
            .catch(err => console.log(err))
    }

    // getToken() {
    //     if (firebase.auth().currentUser != null) {
    //         // firebase.auth().currentUser.getIdToken().then(token => this.token = token).catch(err => console.log(err))
    //         firebase.auth().currentUser.getIdToken()
    //             .then(token => this.store.dispatch(new AuthActions.SetToken(token)))
    //             .catch(err => console.log(err))
    //     }
    //     return this.token;
    // }

    // isAuthenticated() {
    //     return this.token != null;
    // }

    logout() {
        firebase.auth().signOut();
        this.store.dispatch(new AuthActions.Logout())
        this.router.navigate(['/signin']);
        // this.token = null
    }
}