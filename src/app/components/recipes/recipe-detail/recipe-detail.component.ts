import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import * as ShoppingAction from '../../shopping/store/shopping.actions'
import * as RecipesAction from '../../recipes/store/recipe.actions'
import { RecipeState, RecipesFeatureAppState } from 'src/app/store/app.reducers';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  recipeState: Observable<RecipeState>;
  id:number;
  constructor(private route:ActivatedRoute,private router:Router,
    private store:Store<RecipesFeatureAppState>
    ) { }

  ngOnInit() {
    const id=+this.route.snapshot.params['id'];
    this.route.params.subscribe(
      (params:Params)=>{
        this.id=+params['id'];
        this.recipeState=this.store.select('recipes');
      }
    )
    
  }

  onDeleteRecipe(){
    this.store.dispatch(new RecipesAction.DeleteRecipe(this.id));
    this.router.navigate(['/recipes'],{relativeTo:this.route})
  }

  onAddToShoppingList() {
    this.store.select('recipes').take(1).subscribe((recipeState:RecipeState)=>{
      this.store.dispatch(new ShoppingAction.AddIngredients(recipeState.recipes[this.id].ingredients));//NGRX
    })
  }

  onEditRecipe(){
    this.router.navigate(['edit'],{relativeTo:this.route});
    // this.router.navigate(['../',this.id,'edit'],{relativeTo:this.route});
  }

}
