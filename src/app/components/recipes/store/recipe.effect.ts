import { Effect, Actions, ofType } from '@ngrx/effects';
import * as RecipeActions from './recipe.actions'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/switchMap'
import 'rxjs/add/operator/withLatestFrom'
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpEventType, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { Recipe } from '../recipe.model';
import { RecipesFeatureAppState } from 'src/app/store/app.reducers';
import { Store } from '@ngrx/store';

@Injectable()
export class RecipesEffect{

    url = 'https://udemy-angular-shopping-list.firebaseio.com/';

    @Effect()
    recipeFetch = this.actions$.pipe(ofType(RecipeActions.FETCH_RECIPE))
        .switchMap((action:RecipeActions.FetchRecipe)=>{
           return this.http.get<Recipe[]>(`${this.url}recipes.json`,{
                observe:'body', //default is body
                responseType:'json' //default is json can be (blob,arraybuffer)
            })
        }).map(
            (recips)=>{
                // const recips:Recipe[]=res;
                console.log(recips);
                recips.forEach(rec => { // if its empty need to assign to empty array
                    if(!rec['ingredients']){
                        rec['ingredients']=[];
                    }
                });
                return {
                    type:RecipeActions.SET_RECIPES,
                    payload:recips
                }
            }
        );
    
        @Effect({dispatch:false})
        recipeStore = this.actions$.pipe(ofType(RecipeActions.STORE_RECIPE))
            .withLatestFrom(this.store.select('recipes'))
            .switchMap(([action,state])=>{
                const req= new HttpRequest<any>("PUT",`${this.url}recipes.json`,state.recipes,{
                    reportProgress:true,
                    // params:new HttpParams().set('auth',token)//set in iterceptor
                })
                return this.http.request(req);
            })

    constructor(private actions$:Actions,private http:HttpClient,private store:Store<RecipesFeatureAppState>){}
}