import { RecipeState } from 'src/app/store/app.reducers';
import * as RecipeActions from './recipe.actions'


const initialState: RecipeState = {
    recipes: []
}
export function recipeReducer(state = initialState, action: RecipeActions.RecipeActionsType) {
    switch (action.type) {
        case RecipeActions.SET_RECIPES:
            return {
                ...state,
                recipes: [...action.payload]
            }
        case RecipeActions.ADD_RECIPE:
            return {
                ...state,
                recipes: [...state.recipes, action.payload]
            }
        case RecipeActions.UPDATE_RECIPE:
            const recipe = state.recipes[action.payload.index];
            const updatedRecipe = {
                ...recipe,
                ...action.payload.updatedRecipe
            }
            const recipes = [...state.recipes];
            recipes[action.payload.index] = updatedRecipe;
            return {
                ...state,
                recipes: recipes
            }
        case RecipeActions.DELETE_RECIPE:
            const updatedRecipes = [...state.recipes];
            updatedRecipes.splice(action.payload,1);
            return {
                ...state,
                recipes: updatedRecipes
            }

        default:
            return state;
    }
}