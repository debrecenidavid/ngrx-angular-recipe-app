import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipesComponent } from './recipes.component';
import { RecipeStartComponent } from './recipe-start/recipe-start.component';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { AuthGuard } from '../auth/auth-guard.service';

const recipesRoutes:Routes=[
    // {path:'recipes',component: RecipesComponent,canActivate:[AuthGuard], children:[ // cuz of the lazy loading
    {path:'',component: RecipesComponent, children:[ //canActivate replace with can load in higher but can activate still works
        {path:'',component:RecipeStartComponent},
        {path:'new',component:RecipeEditComponent},
        {path:':id',component:RecipeDetailComponent},
        {path:':id/edit',component:RecipeEditComponent},
      ]}
]
@NgModule({
    imports:[RouterModule.forChild(recipesRoutes)],
    exports:[RouterModule] // if export here just like u import in recipes.module
})
export class RecipesRoutingModule{

}