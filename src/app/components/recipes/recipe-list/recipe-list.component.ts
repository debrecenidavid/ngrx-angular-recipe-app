import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { RecipeState, RecipesFeatureAppState } from 'src/app/store/app.reducers';


@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipesState:Observable<RecipeState>;
  constructor(private router: Router, private route: ActivatedRoute, private store: Store<RecipesFeatureAppState>) { }

  ngOnInit() {
    this.recipesState = this.store.select('recipes')
  }
  onNewRecipe() {
    this.router.navigate(['new'], { relativeTo: this.route })
  }
}
