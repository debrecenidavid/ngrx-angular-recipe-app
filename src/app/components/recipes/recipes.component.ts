import { Component, OnInit } from '@angular/core';
import { Recipe } from './recipe.model';
import { Store } from '@ngrx/store';
import { RecipesFeatureAppState } from 'src/app/store/app.reducers';
import * as RecipesActions from './store/recipe.actions'

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css'],
})
export class RecipesComponent implements OnInit {

  selectedRecipe: Recipe;
  constructor(private store: Store<RecipesFeatureAppState>) { }

  ngOnInit() {
    this.store.dispatch(new RecipesActions.FetchRecipe())
  }

}
