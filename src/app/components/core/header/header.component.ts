import { Component, OnInit} from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { AuthState, RecipesFeatureAppState } from 'src/app/store/app.reducers';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as AuthActions from '../../auth/store/auth.actions'
import * as RecipeActions from '../../recipes/store/recipe.actions'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  authState: Observable<AuthState>
  constructor(protected authService:AuthService, private store:Store<RecipesFeatureAppState>) { }

  ngOnInit() {
   this.authState = this.store.select('auth')
  }

  onSave(){
    this.store.dispatch(new RecipeActions.StoreRecipe())
  }
  onLogout(){
    this.store.dispatch(new AuthActions.Logout())
  }

}
