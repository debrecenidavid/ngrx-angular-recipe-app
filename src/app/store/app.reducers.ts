import { ActionReducerMap } from '@ngrx/store';
import { Ingredient } from 'src/app/shared/ingredient.model';
import * as fromShoppingReducer from '../components/shopping/store/shopping.reducers'
import * as fromAuthReducer from '../components/auth/store/auth.reducers'
import { Recipe } from '../components/recipes/recipe.model';

export type AppState = {
  shopping: ShoppingState,
  auth : AuthState
}

export type ShoppingState = {
  ingredients: Ingredient[],
  editedIngredient:Ingredient,
  editedIngredientIndex:number,
}

export interface RecipesFeatureAppState extends AppState{
  recipes: RecipeState
}
export type RecipeState = {
 recipes:Recipe[]
}

export type AuthState = {
  token:string,
  authenticated:boolean
}

export const appReducers:ActionReducerMap<AppState> = {
    shopping:fromShoppingReducer.shoppingReducer,
    auth: fromAuthReducer.authReducer
}