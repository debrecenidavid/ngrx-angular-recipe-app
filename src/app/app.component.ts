import { Component } from '@angular/core';
import * as firebase from 'firebase/app';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  ngOnInit(): void {
    firebase.initializeApp({
      apiKey: "AIzaSyBPC4X5eRzpOLQ0Tc8Unf1vDKye7n4dA7k",
      authDomain: "udemy-angular-shopping-list.firebaseapp.com",
      databaseURL: "https://udemy-angular-shopping-list.firebaseio.com",
      projectId: "udemy-angular-shopping-list",
      storageBucket: "udemy-angular-shopping-list.appspot.com",
      messagingSenderId: "99991669823"
    })
  }
}
